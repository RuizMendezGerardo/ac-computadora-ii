Mapa 1: Computadoras electrónicas
```plantuml
@startmindmap
* Computadoras electrónicas

** Antecedentes
*** En el siglo XX, las computadoras eran de principios específicos (una sola tarea).
*** El considerable aumento de la población aumentaba drásticamente la cantidad de datos.
*** Las cuestiones políticas y sociales de la epoca aumentaron la necesidad de avanzar tecnológicamente.
*** Las computadoras aumentaron de forma notable su tamaño: iniciando del tamaño de un gabinete a ocupar habitaciones enteras.

** Dispositivos clave
*** Relés
**** Es un interruptor mecánico controlado eléctricamente.
**** Está compuesto por una bobina, un brazo de hierro y dos contactos.
**** Cuando una corriente atraviesa la bobina genera un campo magnético, que atrae al brazo y cierra el circuito.
**** Tiene un funcionamiento similar al de una llave de agua, pero con electrones.
**** Se conecta a otros circuitos para hacer operaciones lógicas.
**** Al tener masa, es menos eficiente que sus predecesores
**** Cambiaba de estados cada 50 veces por segundo (50 Hz).
**** Con el tiempo se averia facilmente, carece de fiabilidad
**** En cuantos mayores relés haya mayor posibilidad de avería existe

*** Tubos de vacío
**** Creado en 1904 por John Ambrose Fleming
**** Consiste por un filamento y dos electrodos en un bulbo de cristal
**** Cuando el filamento se calienta, permite el flujo de corriente en un solo sentido
**** Al aplicar una carga en el electrodo de control funciona como un interrumtor, dando el funcionamiento de un relé
**** Al no tener partes moviles, se dañaba menos que el relé
**** Podía cambiar de estado miles de veces por segundo
**** Fue la base de la radio y los telefonos por aproximandamente 50 años
**** Eran frágiles y se podían quemar como los focos
**** Eran costosos

*** Transistores
**** creados en 1947 por John Berdeen, Walter Brattain y William Shockley
**** Al igual que el relé y los tubos de vacío, cumple la funcion de interruptor
**** Estan hechos de silicio, un material aislante que al ser dopado permite la conducción eléctrica
**** Consta de 3 conectores: Colector, emisor y base
**** Cuando hay flujo de corriente entre la base y el emisor permite el flujo de corriente entre el emisor y el colector
**** Podía cambiar de estado 10,000 veces por segundo (10,000 Hertz)
**** Al ser sólidos, son mucho mas resistentes
**** Pueden hacerse mucho mas pequeño que un relé o un tubo de vacio
**** Hoy en día tienen un tamaño menor a 50 nm
**** Pueden cambiar de estado millones de veces por segundo

** Hardvard Mark I
*** construida por IBM en 1944 durante la segunda guerra mundial
*** Constaba de 65,000 componentes, 80 km de cable, un eje de 15 metros y 5 HP
*** Hacía simulaciones y cálculos para el proyecto Manhattan
*** Podía hacer 3 sumas y restas por segundo, multiplicaciones en 6 segundos y divisiones en 15
*** Consta de 3,500 reles, fallando aproximadamente 1 rel al dia
*** Al funciones con Reles, las operaciones complejas tardaban varios minutos
*** Al ser tan grande atraia insectos, creando el termino "bug"

** Colossus Mark I
*** Diseñada por Tommy Flowers en 1943
*** Es conocida como la primer computadora electronica programable
*** Se configuraba mediante conectar cientos de cables en un tablero
*** Fueron construidas 10 colossus en el parque Bletchley, en el Reino unido con el objetivo de decodificar las comunicaciones nazis.
*** Tenia 1,600 tubos de vacío

** ENIAC (lectronic Numerical Integrator And Computer)
*** Diseñada en 1946 en la universidad de Pensilvania, por John Mauchly y J. Presper Eckert
*** Primera computadora electronica programable de proposito general
*** podía realizar 5,000 sumas y restas de 10 dígitos por segundo
*** Operó durante 10 años
*** Al funcionar con tubos de vacío, era solo operada durante la mitad del dia por los problemas en los mismos

** IBM 608
*** lanzada en 1957
*** Es la primera computadora disponible comercialmente basada enteramente en transistores
*** Podía realizar cerca de 4,500 sumas y 80 multiplicaciones o divisiones por segundo
*** Con la llegada de esta computadora, IBM comenzo a usar transistores en todos sus productos, acercándolos a las oficinas y hogares que pudieran costearlas
@endmindmap
```

Mapa 2: Arquitectura Von Neumann y Arquitectura Harvard

```plantuml
@startmindmap
* Arquitectura Von Neumann y Arquitectura Harvard

** Ley de Moore
*** Establece que la velocidad o poder del procesador se duplica cada doce meses

*** Parte electrónica
**** Se sabe que la cantidad de transistores también se duplica cada año
**** A pesar de las mejorías, el costo no cambia
**** Caad 18 semes se duplica la potencia de calculo sin modificar el costo

*** Performance
**** La velocidad del procesador aumenta
**** La capacidad de la memoria incrementa
**** La velocidad de la memoria corre siempre por detrás de la del proccesador

** Funcionamiento de una computadora
*** Sistemas antiguos
**** Eran sistemas cableados, se enchufaban los cables para programar a nivel hardware
**** Tenía una entrada de datos, una secuencia lógica y devolvía un resultado

*** Sistemas actuales
**** La programación se hace mediante software
**** Con transistores creamos las compuertas lógicas para darle ordenes
**** Tiene una entrada de datos, una secuencia logica con un interprete de instrucciones y devuelve un resultado

** Arquitectura Von Neumann
*** Esta arquitectura esta basada en lo descrito por John Von Neumann en 1945, en el primer borrador de un informe sobre el EDVAC
*** Los datos y programas se almacenan en una misma memoria de lectura-escritura
*** Los contenidos en esta memoria se acceden indicando su posición sin importar su tipo
*** Tiene una ejecución en seciencia (a excepción de que se le indique lo contrario)
*** Es representada en binario
*** Gracias a esta arquitectura surge el concepto de "Programa almacenado"
*** Al tener separada la memoria y la CPU apareció el problema del "Cuello de botella", por que la cantidad de datos que pasan entre estos elementos difiere mucho con las velocidades de los mismos.
*** Consta de 3 partes fundamentales

**** Unidad central de procesamiento (CPU)
***** A su vez, esta se divide en 3
****** Unidad de control
****** Unidad aritmética Lógica
****** Registros

**** Memoria principal
***** Puede almacenar tanto datos como instrucciones

**** Sistema de entrada/salida
***** Mantienen comunicados a la computadora y al usuario

*** Consta de un sistema de buses que mantiene comunicadas a las demás partes
**** Bus de control
**** Bus de direcciones
**** Bus de datos e instrucciones

*** La funcion de una computadora es la ejecución de programas por medio de instrucciones
**** Estas instrucciones son ejecutadas por la CPU a travez de un ciclo denominado ciclo de instrucciones
***** Este consta de 3 pasos: Fetch, decode y execute
**** Consisten en secuencias binarias llamadas código máquina (el cual no es leible por personas)
**** Es por esto que existen lenguajes de bajo nivel (como ensamblador) y de alto nivel (como Java)
**** Por ejemplo, 3,000,000,000 de operaciones por segundo equivale a  GHz
*** Actualmente siguen usandose los principios de esta arquitectura

** Arquitectura Hardvard
*** A diferencia de la arquitectura Vonn Neumann, esta se usa para computadoras con dispositivos de almacenamiendo físicamente separados para las instrucciones y para los datos
*** Al igual que la arquitectura Neumann, consta de 3 partes:
**** Unudad de procesamiento (CPU)
***** Unidad de control (UC)
***** Unidad aritmética y lógica (Alu)
**** Memoria principal
***** Memoria de instrucciones, para almacenar las instrucciones que debe ejecutar el miscrocontrolador
***** Memoria de datos, que almacena los datos almacenados por los programas.
**** Sistema de entrada y salida

*** Problemas de memoria
**** Al existir el problema del precio al construir memorias mas rápidas, se crean las memorias cache
***** Estas memorias son de menor tamaño que las memorias principales, pero son mucho mas rapidas. En ellas se guardan los datos que necesita el procesador
**** La solucion de Hardvard es almacenar las instrucciones y datos en cachés separadas para mejorar el rendimiento
***** La deventaja es que la memoria caché total se tiene que dividir en ambas cachés

*** Esta arquitectura suele usarse en PICs o miscrocontroladores, para productos de propósito específico
@endmindmap
```

Mapa 3: Basura electrónica

```plantuml
@startmindmap
* Basura electrónica

** Se define como basura electrónica a los resultandes de cualquier aparato electrónico que termino su vida útil
*** Por las siglas REELE, es definido como "Residuos de los aparatos electrónicos y eléctricos"
** Ejemplos de estos son Computadoras, televisiones, celulares, etc.

** Reciclaje
*** Para reciclarlos, se sigue un proceso:
**** Se recolectan
**** Se separan dependiendo su categoría
**** Se desarman y clasifican
**** Se reutilizan lo mayor posible

*** El mayor estímulo para reciclarlo es el reutilizar los metales preciosos que se encuentran en estos residuos, como son:
**** Oro
**** Plata
**** Cobre
**** Cobalto

*** Plantas
**** Existen alrededor del mundo centros especializados para la recoleccion de estos, aunque al ser un proceso complicado y caro no son tan comunes
**** Algunos gobiernos están tomando cartas en el asunto. Por ejemplo, la Unión Europea exige que cada pais recoja 45 toneladas de residuos electronicos por cada 100 de productros puestos a la venta

*** Mercado negro
**** Existe un mercado ilegal a la hora de manejar la basura electrónica
**** Al ser tan caro el proceso, muchas empresan optan por enviar los residuos a paises del tercer mundo
**** Estas empresas ponen a trabajar a los empleados en condiciones infrahumanas por los gases expulsados por los dispositivos
**** Estos gases reducen mucho la esperanza de vida de las personas
**** Además de los metales, muchos buscan robar la información personal almacenada en los discos duros y así suplantar la identidad de las personas

** Consecuencias del mal desecho
*** Al no deshacerte adecuadamente de la basura electrónica, puede traer males al ambiente
*** El 70% de las toxinas de los tiraderos proviene de aparatos electrónicos
*** Una sola pila puede contaminar 600 mil litros de agua
*** Son uno de los principales contaminantes de agua y tierra

** Tesoros
*** Para muchas personas, la basura electrónica es buen lugar para encontrar "tesoros ocultos"
*** Muchos de estos productos son buscados por el factor nostalgia, dando una vista al pasado
*** Además, algunos dispositivos aún pueden ser reparados y volver a usarse con normalidad

@endmindmap
```